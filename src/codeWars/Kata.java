package codeWars;

public class Kata {
    public static int squareSum(int[] n) {
        int sum = 0;
        for (int i = 0; i < n.length; i++) {

            sum += n[i] * n[i];
        }
        System.out.println(sum);
        return sum;
    }

    public static void main(String[] args) {
        squareSum(new int[]{1, 2, 3, 4});
    }
}
