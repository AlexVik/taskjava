package codeWars.Task;

import com.sun.org.apache.xpath.internal.functions.FuncContains;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TextAnalysis {
    //private static final String PUNCT = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~,«»";

    public static String removePunct(String str) {
        StringBuilder result = new StringBuilder(str.length());
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isAlphabetic(c) || Character.isDigit(c) || Character.isSpaceChar(c)) {
                result.append(c);
            }
        }
        //System.out.println(result);
        return result.toString();
    }
    public static void informationOnText(String text){
        String[] words = text.split("[\\s]");
        Map<String, Integer> mapWords = new HashMap<>();
        for (String word : words) {
            mapWords.put(word, mapWords.getOrDefault(word, 0) + 1);
        }
        mapWords.forEach((k, v) -> System.out.println("Слово: " + "|" + k + "|" + " всречается: " +  v +  " раз/а"));
        System.out.println("Всего слов в тексте: " + words.length);
            }

public static void main(String[] args) {

        informationOnText(removePunct("вполголоса: «Генерал-поручик!.. " +
                "Он у меня в роте был сержантом!.. " +
                "Обоих российских орденов кавалер!.. " +
                "А давно ли мы...» он был сержантом и был кавалер россиских орденов"));
    }

}



