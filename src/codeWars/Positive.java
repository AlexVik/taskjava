package codeWars;

public class Positive {
    public static int sum(int[] arr) {
        int sumArrys = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                arr[i] = 0;
            } else {
                sumArrys += arr[i];
            }
        }
        System.out.println(sumArrys);
        return sumArrys;
    }

    public static void main(String[] args) {

        sum(new int[]{1, -5, 6, 7, -3});

    }


}
