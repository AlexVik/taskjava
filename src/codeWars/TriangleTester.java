package codeWars;

public class TriangleTester {
    public static boolean isTriangle(int a, int b, int c) {
        boolean result = true;
        if (a + b > c && a + c > b && b + c > a) {
            return result;

        } else {
            result = false;
        }
        System.out.println(result);
        return result;
    }

    public static void main(String[] args) {
        isTriangle(5, 6, 7);
        isTriangle(10, 10, 11);
    }
}
