package codeWars.Task;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class FieldOfDreams {
    String[] words;

    public static void guessTheWord(String[] words) {

        System.out.println(Arrays.toString(words));
        System.out.println("Угадайте слово которое задумал компьютер\nВводите по одной букве  ");
        Random randomWord = new Random();
        int index = randomWord.nextInt(words.length);
        System.out.println(words[index]);
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Введите букву: ");
            String ch = in.next();
            String word = words[index];
            char[] arrayOfLetters = word.toCharArray();
            char[] arrayOfCh = ch.toCharArray();

            if (ch.equalsIgnoreCase(word)) {
                System.out.println("Поздравляем вы угадали!\nЗагаданное слово: " + words[index]);
                break;
            } else {
                String guessedWord = "";

                for (int i = 0; i < arrayOfLetters.length; i++) {
                    if (i < arrayOfCh.length && arrayOfCh[i] == arrayOfLetters[i]) {
                        guessedWord += arrayOfCh[i];
                    } else guessedWord += "#";
                }
                System.out.println(guessedWord);
            }
        }
    }

    public static void main(String[] args) {
        guessTheWord(new String[]{"apple", "orange", "lemon", "banana", "apricot",
                "avocado", "broccoli", "carrot", "cherry", "garlic", "grape",
                "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive",
                "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"});

    }
}
