package codeWars;

public class NumberToString {
    public static String numberToString(int num) {
        String str = String.valueOf(num);
        return str;
    }

    public static void main(String[] args) {

        System.out.println(numberToString(157));
    }
}
